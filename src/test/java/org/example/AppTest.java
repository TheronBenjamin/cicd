package org.example;

import junit.framework.TestCase;
import org.junit.jupiter.api.*;

/**
 * Unit test for simple App.
 */
public class AppTest
    extends TestCase
{


    /**
     * Rigourous Test :-)
     */
    @Test
    public void testApp()
    {
        assertTrue( true );
    }

    /**
     * Rigourous Test :-)
     */
    @Test
    public void test2()
    {
        String color = "blue";
        assertEquals("blue", color);
    }

    @Test
    public void test3(){
        int number = 6;
        assertEquals(2 * 3, number);
    }

    @Test
    public void test4(){
        int number = 6;
        assertEquals(3 + 3, number);
    }
}
